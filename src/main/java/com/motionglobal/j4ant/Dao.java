package com.motionglobal.j4ant;

import com.mysql.jdbc.Connection;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.commons.configuration2.Configuration;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

class Dao {

    private static final Logger logger = LogManager.getLogger(Dao.class);

    static String ExecuteQuery(String sql) {

        try {
            Configuration config = Utilities.GetConfig();

            String url = "jdbc:mysql://%s:%s/%s";
            String host = config.getString("jdbc.host");
            String port = config.getString("jdbc.port");
            String schema = config.getString("jdbc.schema");
            String username = config.getString("jdbc.username");
            String password = config.getString("jdbc.password");
            String connString = String.format(url, host, port, schema);

            int resultLimit = config.getInt("jdbc.result.limit");

            Connection conn = (Connection) DriverManager.getConnection(connString, username, password);
            ResultSet rs = conn.createStatement().executeQuery(sql);

            StringBuilder result = new StringBuilder();

            // get column info
            ResultSetMetaData meta = rs.getMetaData();
            int columnCount = meta.getColumnCount();

            result.append("#");
            for (int i = 1; i <= columnCount; i++) {
                result.append("\t").append(meta.getColumnLabel(i));
            }

            int rowCount = 0;

            while (rs.next()) {
                result.append("\n");
                result.append(++rowCount);
                for (int i = 1; i <= columnCount; i++) {
                    result.append("\t").append(rs.getString(i));
                }

                if (rowCount >= resultLimit)
                    break;
            }

            rs.close();
            conn.close();

            return result.toString();

        } catch (SQLException e) {
            String sb = "SQLException: " + e.getMessage() + "\n" +
                    "ErrorCode: " + e.getErrorCode() + "\n" +
                    "SQLState: " + e.getSQLState() + "\n";
            logger.error(sb);
            return "Database error. " + e.toString();
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage() + "\n");
            return "Unexpected error. " + e.toString();
        }
    }
}
