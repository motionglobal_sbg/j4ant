package com.motionglobal.j4ant;

import java.util.Arrays;

public class Processor {
    public static void main(String[] args) {

        String result;

        if ((args != null) && (args.length > 1)) {
            String callMethod = args[0];

            switch (callMethod) {
                case "AppendProperties":
                    String[] properties = Arrays.copyOfRange(args, 1, args.length);
                    result = Utilities.AppendProperties(properties);
                    break;
                case "ExecuteQuery":
                    result = Dao.ExecuteQuery(args[1]);
                    break;
                case "ConvertToHtmlTable":
                    result = Utilities.ConvertToHtmlTable(args[1]);
                    break;
                case "SendMail":
                    result = Utilities.SendMail(args[1], args[2], args[3], args[4]);
                    break;
                default:
                    result = "Invalid method name.";
                    break;
                }
        } else {
            throw new IllegalArgumentException("Invalid args. Must provide at least 2 args.");
        }

        System.out.println(result);
    }
}
