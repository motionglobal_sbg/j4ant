package com.motionglobal.j4ant;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Utilities {

    private static final Logger logger = LogManager.getLogger(Utilities.class);

    static String AppendProperties(String[] properties) {
        StringBuilder result = new StringBuilder();

        if (properties != null) {
            for (String property : properties)
                result.append(property);

            return result.toString();
        } else {
            return "";
        }
    }

    static Configuration GetConfig()  {
        Configuration config = null;

        Configurations configs = new Configurations();
        String configFilePath1 = System.getProperty("user.dir") + "/config.properties";
        String configFilePath2 = System.getProperty("user.dir") + "/src/main/resources/config.properties";
        File configFile1 = new File(configFilePath1);
        File configFile2 = new File(configFilePath2);

        try {
            if (configFile1.exists() && !configFile1.isDirectory())
                config = configs.properties(configFile1);
            else if (configFile2.exists() && !configFile2.isDirectory())
                config = configs.properties(configFile2);

        } catch (ConfigurationException e) {
            logger.error("Configuration error. " + e.getMessage());
        }

        return config;
    }

    static String ConvertToHtmlTable(String rawText) throws IllegalArgumentException {

        if (rawText == null || rawText.length() == 0) {
            String errorMessage = "Null or empty raw text to convert.";
            throw new IllegalArgumentException(errorMessage);
        }

        rawText = rawText.trim();

        String[] lines = rawText.split("\\n");
        int lineCount =  lines.length;

        // if only 1 line (table header), then ignore converting
        if (lineCount < 2)
            return "";

        StringBuilder result = new StringBuilder();
        result.append("<table>");

        for (int i = 0; i < lineCount; i++) {
            result.append("<tr>");
            String[] columns = lines[i].trim().split("\\t");
            for (String column : columns) {

                String value = column;
                String pattern = "<%s>%s</%s>";
                String cellTag = i == 0 ? "th" : "td";

                if (Utilities.isNumeric(value) && cellTag.equals("td")) {
                    pattern = "<%s class='num'>%s</%s>";
                } else if (value.equals("null") && cellTag.equals("td")) {
                    pattern = "<%s class='nul'>%s</%s>";
                    value = "NULL";
                }

                result.append(String.format(pattern, cellTag, value, cellTag));
            }
            result.append("</tr>");
        }
        result.append("</table>");

        return result.toString();
    }

    private static boolean isNumeric(String text) {
        String patternString = "^-?(\\d{1,3}(,\\d{3})+|\\d+)(\\.\\d+)?%?$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    static String SendMail(String emailTo, String emailCc, String subject, String htmlText) {

        Configuration config = Utilities.GetConfig();
        String fromName = config.getString("mail.fromName");
        String emailFrom = config.getString("mail.from");
        String sendGridApiKey = config.getString("mail.apiKey");
        SendGrid sendgrid = new SendGrid(sendGridApiKey);

        SendGrid.Email email = new SendGrid.Email();
        email.addTo(emailTo.trim().split(";"));
        email.addCc(emailCc.trim().split(";"));
        email.setFrom(emailFrom);
        email.setFromName(fromName);
        email.setSubject(subject.trim());

        if (htmlText.trim().length() > 0) {

            htmlText = String.format("<style>%s</style>%s", config.getString("mail.style"), htmlText);
            email.setHtml(htmlText);
            logger.debug(htmlText);

            try {
                SendGrid.Response response = sendgrid.send(email);
                String responseMsg = response.getMessage();

                logger.info(responseMsg);
                return responseMsg;
            }
            catch (SendGridException e) {
                String msg = e.getMessage();
                logger.error(msg);
                return e.getMessage();
            }
        } else {
            logger.warn("Empty mail content. Ignore sending out the mail.");
            return "";
        }
    }
}