package com.motionglobal.j4ant;

import org.junit.Test;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DaoTest {

    @BeforeClass
    public static void setUp() {
        System.out.println("Entering Dao Test...");
    }

    @AfterClass
    public static void tearDown() {
        System.out.println("Exiting Dao Test.");
    }

    @Test
    public void testExecuteQuery() throws Exception {
        System.out.println("Start to run test ExecuteQuery...");

        String result = Dao.ExecuteQuery("select country_id, country_name from sbg_country limit 20;");
        System.out.println(result);

        assertNotNull("Retrieve data from database failed.", result);
    }
}
