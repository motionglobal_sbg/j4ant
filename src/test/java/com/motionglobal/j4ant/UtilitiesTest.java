package com.motionglobal.j4ant;

import org.junit.Test;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UtilitiesTest {

    @BeforeClass
    public static void setUp() {
        System.out.println("Entering Dao Test...");
    }

    @AfterClass
    public static void tearDown() {
        System.out.println("Exiting Dao Test.");
    }

    @Test
    public void testConvertToHtmlTable() throws Exception {
        System.out.println("Start to test ConvertToHtmlTable...");

        String rawData = Dao.ExecuteQuery("select country_id, country_name from sbg_country limit 20;");
        String result = Utilities.ConvertToHtmlTable(rawData);
        System.out.println(result);

        assertNotNull("Convert text to HTML table failed.", result);
    }

    @Test
    public void testSendMail() {
        System.out.println("Start to test SendMail...");

         String rawData = Dao.ExecuteQuery("select country_name, lens_care_kit, carrier_value from sbg_country limit 10;");
         String mailContent = Utilities.ConvertToHtmlTable(rawData);
         String result = Utilities.SendMail("wayne.chen@motionglobal.com", "", "Test", mailContent);
         System.out.println(result);

         assertNotNull("Sending mail failed.", result);
    }

    @Test
    public void testAppendProperties() {
        System.out.println("Start to test AppendProperties...");

        String[] properties = new String[] {"AppendProperties", "Hello", " ", "World", "!"};

        Processor.main(properties);

        assertTrue("Result not match with expectation.", true);
    }
}
